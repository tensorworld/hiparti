import HiParTIPy.Matrix as mat
import HiParTIPy.Vector as vec
import HiParTIPy.Buffers as buff
import sys
import time

name = sys.argv[1]
niters = int(sys.argv[2])
eqi = int(niters/100)

coo = mat.COOMatrix()
coo.loadMatrix(name)
hicoo=coo.convertToHiCOO()
csr = coo.convertToCSR()

vec = vec.ValueVector(coo.ncols())
vec.makeRandom()

buf = buff.VecBuff(coo.ncols())



for i in range(eqi):
    coo.multiplyVectorBuff(vec,buf,True)
start=time.time()
for i in range(niters):
    coo.multiplyVectorBuff(vec,buf,True)
end=time.time()
print("COO: niters = {}\nTot Time = {}\n Time per Cycle = {}".format(niters,end-start,(end-start)/niters))
buf.free()
buf = buff.VecBuff(hicoo.ncols())
for i in range(eqi):
    hicoo.multiplyVectorBuff(vec,buf,True)
start=time.time()
for i in range(niters):
    hicoo.multiplyVectorBuff(vec,buf,True)
end=time.time()
print("HiCOO: niters = {}\nTot Time = {}\n Time per Cycle = {}".format(niters,end-start,(end-start)/niters))
buf.free()
buf = buff.VecBuff(csr.ncols())
for i in range(eqi):
    csr.multiplyVectorBuff(vec,buf,True)
start=time.time()
for i in range(niters):
    csr.multiplyVectorBuff(vec,buf,True)
end=time.time()
print("CSR: niters = {}\nTot Time = {}\n Time per Cycle = {}".format(niters,end-start,(end-start)/niters))
buf.free()
